<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});


Route::get('report', 'App\Http\Controllers\ReportController@index')->name('report');
Route::get('save', 'App\Http\Controllers\saveController@index')->name('save');
Route::get('cekid', 'App\Http\Controllers\CekidController@index')->name('cekid');
Route::get('reportbug', 'App\Http\Controllers\ReportBugController@index')->name('landing_page');

Route::get('template', 'App\Http\Controllers\DashboardController@template')->name('template');

Route::get('dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');

Route::get('f2', 'App\Http\Controllers\F2Controller@index')->name('f2');
    // f3
Route::get('f3', 'App\Http\Controllers\F3Controller@index')->name('f3');