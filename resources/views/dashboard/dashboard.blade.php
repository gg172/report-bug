<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard | GI Report</title>
    <link rel="stylesheet" href="{{ asset('assets/img/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <!-- <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body style="height: 100%;">
    <div class="d-flex global-container">
        <div class="wrapper  sidenav " id="sidenav-1" data-mdb-hidden="false">
            <header class="sidebar bg-secondary p-2" style="min-width:20rem; min-height: 63rem;">
                <div class="profile  rounded p-1 d-flex justify-center align-items-center text-white mt-2 h-100 ">
                    <img src="{{ asset('assets/img/profile.jpg') }}" class="rounded-circle" height="75px" width="75px" alt="" srcset="">
                    <div class="ms-3  name mt-3">
                        <h6>Fahri Azis Purnomo</h5>
                            <p class="fw-lighter">Supervisor</p>

                    </div>
                </div>

                <h6 class="text-white text-center fw-light mt-5 mb-4"> MAIN MENU </h6>
                <hr class="text-white ">
                <ul class=" " style="list-style: none;">
                    <li><a href="dashboard" class="text-decoration-none btn mt-4 btn-light d-flex " style="width:15rem;"><i class="bi bi-speedometer2 me-2"></i>Dashboard</a></li>
                    <li><a href="#" class=" btn btn-light mt-4 d-flex" style="width:15rem;"><i class="bi bi-person me-2"></i>Profile</a></li>
                    <li><a href="#" class=" btn btn-light mt-4 d-flex" style="width:15rem;"><i class="bi bi-person-circle me-2"></i>Karyawan</a></li>
                    <li><a href="#" class=" btn btn-light mt-4 d-flex" style="width:15rem;"><i class="bi bi-person-badge me-2"></i>Ts</a></li>

                    <button type="button" style="width:15rem;" class="btn btn-side  d-flex text-center mt-4" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                        <i class="bi bi-box-arrow-right me-2"></i>Logout
                    </button>
                    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-warning ">
                                    <h1 class="modal-title fs-5  " id="staticBackdropLabel"><i class="bi bi-exclamation-triangle-fill"></i> Logout</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    Are you sure?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <a href="logout" type="button" class="btn btn-danger">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>

            </header>
        </div>

        <div class=" " style="width: 100%;" id="report-data">
            <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
                <button id="sidenav-btn" data-mdb-toggle="sidenav" data-mdb-target="#sidenav-1" class="btn pt-2 my-1 " aria-controls="#sidenav-1" aria-haspopup="true">
                    <i class="bi bi-list"></i>
                </button>
                <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </header>
            <h4 class="fw-bold mt-3 mb-4 me-5 text-center" style="color: #6c6c6c;">REPORT DATA</h4>
            <hr>
            <p class="text-decoration-underline ms-4">Status Report</p>
            <!-- Status Report -->
            <div class="d-flex test " style="margin-left: 125px;">
                <div class="col-xl-3 col-md-6 mb-4 ">
                    <div class=" card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="d-flex ">
                                    <h1 class="mt-2"><i class="bi bi-check2-circle"></i></h1>
                                    <div class="text-center col mr-2 ">
                                        <div class=" mb-1">
                                            <p class="text-decoration-underline ">Accepted</p>
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">300</div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="  col-xl-3 col-md-6 mb-4 " style="margin-left: 125px;">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="d-flex ">
                                    <h1 class="mt-2"><i class="bi bi-exclamation-octagon"></i></h1>
                                    <div class="text-center col mr-2 ">
                                        <div class=" mb-1">
                                            <p class="text-decoration-underline ">In Progress</p>
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">100</div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=" col-xl-3 col-md-6 mb-4 me-2" style="margin-left: 125px;">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="d-flex ">
                                    <h1 class="mt-2"><i class="bi bi-check2-all"></i></h1>
                                    <div class="text-center col mr-2 ">
                                        <div class=" mb-1">
                                            <p class="text-decoration-underline ">Done</p>
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">2119</div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- PALING SERING REPORT -->
            <p class="text-decoration-underline ms-4">Aplikasi Paling Sering Di Report </p>
            <div class="d-flex test " style="margin-left: 125px;">
                <div class=" col-md-2 mb-4 ">
                    <div class=" card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="d-flex">
                                        <h1> <i class="bi bi-app me-2"></i> </h1>
                                        <div class="ico">
                                            <div class=" text-secondary mb-1">
                                                (Nama Web/Apk)</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">600</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-md-2 mb-4 me-2" style="margin-left: 7.385rem;">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="d-flex">
                                        <h1> <i class="bi bi-app me-2"></i> </h1>
                                        <div class="ico">
                                            <div class=" text-secondary mb-1">
                                                (Nama Web/Apk)</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">232</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-md-2 mb-4 me-2" style="margin-left: 7.385rem;">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="d-flex">
                                        <h1> <i class="bi bi-app me-2"></i> </h1>
                                        <div class="ico">
                                            <div class=" text-secondary mb-1">
                                                (Nama Web/Apk)</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">123</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-md-2 mb-4 me-2" style="margin-left: 7.385rem;">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="d-flex">
                                        <h1> <i class="bi bi-app me-2"></i> </h1>
                                        <div class="ico">
                                            <div class=" text-secondary mb-1">
                                                (Nama Web/Apk)</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">122</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- karyawan sering merespon -->

            <p class="text-decoration-underline  ms-4">Karyawan Paling Sering Merespon</p>
            <div class="d-flex ">
                <div class="test mt-4" style="margin-left: 125px; width: 35%;">
                    <div class="col-xl-8 col-md-6 mb-4 ">
                        <div class=" card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nama Karyawan</div>
                                        <div class=" mb-0 fw-lighter text-gray-800">900 tanggapan</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="  col-xl-8 col-md-6 mb-4 ">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Nama Karyawan</div>
                                        <div class="     mb-0 fw-lighter text-gray-800">800 tanggapan</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=" col-xl-8 col-md-6 mb-4 me-2">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Nama Karyawan</div>
                                        <div class=" mb-0 fw-lighter text-gray-800">700 tanggapan</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card col-md-6">
                    <div class="card-body">
                        <h5 class="">Last Week Report</h5>
                        <div>
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>





            <!-- Earnings (Monthly) Card Example -->

            <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
            <script>
                const ctx = document.getElementById('myChart');

                new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                        datasets: [{
                            label: 'Report',
                            data: [12, 19, 3, 5, 2, 3, 22],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                                'rgba(255, 205, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 205, 86, 0.2)'


                            ],
                            borderColor: [
                                'rgb(255, 99, 132)',
                                'rgb(255, 159, 64)',
                                'rgb(255, 205, 86)',
                                'rgb(75, 192, 192)',
                                'rgb(54, 162, 235)',
                                'rgb(153, 102, 255)',
                                'rgb(255, 205, 86)'


                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            </script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
            <script src="{{ asset('assets/js/script.js') }}"></script>
            <script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>