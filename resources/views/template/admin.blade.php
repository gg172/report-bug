<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard | GI Report</title>
    <link rel="stylesheet" href="{{ asset('assets/img/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <!-- <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body style="">
    <div class="d-flex global-container " style="min-height: 100%;" >
        <div class="wrapper  sidenav " id="sidenav-1" data-mdb-hidden="false">
            <header class="sidebar bg-secondary p-2" style="min-width:20rem; min-height: 63rem;">
                <div class="profile  rounded p-1 d-flex justify-center align-items-center text-white mt-2 h-100 ">
                    <img src="{{ asset('assets/img/profile.jpg') }}" class="rounded-circle" height="75px" width="75px" alt="" srcset="">
                    <div class="ms-3  name mt-3">
                        <h6>Fahri Azis Purnomo</h5>
                            <p class="fw-lighter">Supervisor</p>

                    </div>
                </div>

                <h6 class="text-white text-center fw-light mt-5 mb-4"> MAIN MENU </h6>
                <hr class="text-white ">
                <ul class=" " style="list-style: none;">
                    <li><a href="dashboard" class="text-decoration-none btn mt-4 btn-light d-flex " style="width:15rem;"><i class="bi bi-speedometer2 me-2"></i>Dashboard</a></li>
                    <li><a href="#" class=" btn btn-light mt-4 d-flex" style="width:15rem;"><i class="bi bi-person me-2"></i>Profile</a></li>
                    <li><a href="#" class=" btn btn-light mt-4 d-flex" style="width:15rem;"><i class="bi bi-person-circle me-2"></i>Karyawan</a></li>
                    <li><a href="#" class=" btn btn-light mt-4 d-flex" style="width:15rem;"><i class="bi bi-person-badge me-2"></i>Ts</a></li>

                    <button type="button" style="width:15rem;" class="btn btn-side  d-flex text-center mt-4" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                        <i class="bi bi-box-arrow-right me-2"></i>Logout
                    </button>
                    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-warning ">
                                    <h1 class="modal-title fs-5  " id="staticBackdropLabel"><span class="material-symbols-outlined me-2">warning</span>Logout</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    Are you sure?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <a href="logout" type="button" class="btn btn-danger">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>

            </header>
        </div>

        <div class=" " style="width: 100%;" id="report-data">
            <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
                <button id="sidenav-btn" data-mdb-toggle="sidenav" data-mdb-target="#sidenav-1" class="btn pt-2 my-1 " aria-controls="#sidenav-1" aria-haspopup="true">
                    <i class="bi bi-list"></i>
                </button>
                <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </header>

            <div class="contents">
                @yield('konten')
            </div>


            <script src="{{ asset('assets/js/bootsrap.min.js')}}"></script>
            <script src="{{ asset('assets/js/script.js') }}"></script>
            <script src="{{ asset('assets/js/app.js') }}"></script>
</body>

</html>