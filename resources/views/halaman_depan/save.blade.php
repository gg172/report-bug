<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <script src="assets/js/bootstrap.js"></script>
    <title>saverenponse</title>
    <style>
        body{
            font-family:verdana;
        }
        .card{
            align-items: center;
            width: 30%;
            margin-top: 10%;
            margin-left: 33%;
            border-radius: 20px;
        }
        a{
            text-decoration: none;
        }
        input[type="text"]{
            border-radius: 10px;
        }

        .btn{
            border:2px solid black;
            border-radius: 10px;
        }
    </style>
</head>
<body>
    <form>
        <div class="card p-3 text-center bg-secondary">
            <h5 class=" font-weight-bold">Thank You</h5>
            <p>You have reported a problem with your application. We will solve your application problems as soon as possible, <b>and your ticket id is :</b></p>
            <h6><b>090909</b></h6>
            <a button type="submit" class="btn btn-secondary btn-sm mt-3" data-bs-toggle="modal" data-bs-target="#identity">CHECK ID</a>
        </div>
    </form>
    <!-- modal -->
    <div class="modal fade" id="identity" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content bg-dark-subtle">
                <div class="">
                    <h1 class="modal-title fs-5 text-center pt-2" id="exampleModalLabel">Check Ticket</h1>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-grup mt-3">
                            <input type="text" name="username" class="form-control text-center" placeholder="Enter the ticket ID">
                        </div>

                        <div class="d-grid gap-2 mt-3">
                            <a href="{{ route('f3') }}" button type="submit" class="btn btn-secondary btn-sm mt-3">CHECK</a>
                        </div>
                    </form>
                </div>
                <div class="text-center pb-3">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
</body>
</html>