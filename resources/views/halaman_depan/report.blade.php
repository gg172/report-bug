<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <script src="assets/js/bootstrap.js"></script>
    <title>reportpage</title>
    <style>
        input[type="text"]{
            border-radius: 15px;
        }
        input[type="file"]{
            border-radius: 15px;
        }
        .card{
            width: 45%;
            margin-bottom: 5%;
            margin-top:2%;
            margin-left: 25%;
            border-radius: 5px;
        }
        .card-header{
            width: 100%;
        }
        .card-footer{
            width: 100%;
        }
        .btn{
            border:2px solid black;
            border-radius: 10px;
        }
        a{
            text-decoration: none;
        }
        .row{
            margin-top:1px;
        }
    </style>
</head>
<body>
    <div class="alert alert-secondary alert-dismissible fade show m-2" role="alert">
        already got ticket id <strong><a data-bs-toggle="modal" data-bs-target="#identity">click here</a></strong> to see the progress of solving your problem
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <div class="card ">
        <div class="card-header text-center bg-secondary">
            <h3><b>REPORT HERE</b></h3>
        </div>
        <div class="card-body bg-dark-subtle">
            <form action="">
                <h5><b><u>IDENTITY</u></b></h5>
                <div class="row g-3 align-items-center">
                    <div class="col-auto me-auto">
                        <label for="disabledTextInput" class="col-form-label"><b>Name</b></label>
                    </div>
                    <div class="col-9">
                        <input type="text" id="disabledTextInput" name="name" raquired class="form-control">
                    </div>
                </div>
                <div class="row g-3 align-items-center">
                    <div class="col-auto me-auto">
                        <label for="disabledTextInput" class="col-form-label"><b>Phone</b></label>
                    </div>
                    <div class="col-9">
                        <input type="text" id="disabledTextInput" name="name" raquired class="form-control">
                    </div>
                </div>
                <div class="row g-3 align-items-center">
                    <div class="col-auto me-auto">
                        <label for="disabledTextInput" class="col-form-label"><b>E-mail</b></label>
                    </div>
                    <div class="col-9">
                        <input type="text" id="disabledTextInput" name="name" raquired class="form-control">
                    </div>
                </div>
                <div class="row g-3 align-items-center">
                    <div class="col-auto me-auto">
                        <label for="disabledTextInput" class="col-form-label"><b>Egency</b></label>
                    </div>
                    <div class="col-9">
                        <input type="text" id="disabledTextInput" name="name" raquired class="form-control">
                    </div>
                </div>
                
                <h5 class="mt-3"><b><u>PROBLEM DESCRIPTION</u></b></h5>
                <div class="row g-3 align-items-center">
                    <div class="col-auto me-auto">
                        <label for="disabledTextInput" class="col-form-label"><b>System</b></label>
                    </div>
                    <div class="col-9">
                        <select name="nama_tanaman" class="form-select" style="border-radius: 15px;">
                            <option value="">Select system</option>
                            <option>--------------</option>
                            <option>--------------</option>
                            <option>--------------</option>
                        </select>
                    </div>
                </div>
                <div class="row g-3 align-items-center">
                    <div class="col-auto me-auto">
                        <label for="disabledTextInput" class="col-form-label"><b>Description</b></label>
                    </div>
                    <div class="col-9">
                    <textarea name="" id="" class="col-12" rows="5" style="border-radius: 15px;" placeholder="  write your description here" class="mt-1 mb-1"></textarea>
                    </div>
                </div>
                <div class="row g-3 align-items-center">
                    <div class="col-auto me-auto">
                        <label for="disabledTextInput" class="col-form-label"><b>Description</b></label>
                    </div>
                    <div class="col-9">
                    <div class="container text-center">
                        <div class="row">
                            <div class="col order-last">
                            <input type="file" name="name" raquired class="form-control">
                            </div>
                            <div class="col">
                            <input type="file" name="name" raquired class="form-control">
                            </div>
                            <div class="col">
                            <input type="file" name="name" raquired class="form-control">
                            </div>
                            <div class="col order-first">
                            <input type="file" name="name" raquired class="form-control">
                            </div>
                        </div>
                    </div>                
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer text-center bg-secondary">
            <a href="{{ route('save') }}" class="btn  btn-secondary">SEND</a>
        </div>
    </div>
    <!-- modal -->
    <div class="modal fade" id="identity" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content bg-dark-subtle">
                <div class="">
                    <h1 class="modal-title fs-5 text-center pt-2" id="exampleModalLabel">Check Ticket</h1>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-grup mt-3">
                            <input type="text" name="username" class="form-control text-center" placeholder="Enter the ticket ID">
                        </div>

                        <div class="d-grid gap-2 mt-3">
                            <a href="{{ route('f3') }}" button type="submit" class="btn btn-secondary btn-sm mt-3">CHECK</a>
                        </div>
                    </form>
                </div>
                <div class="text-center pb-3">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
</body>
</html>