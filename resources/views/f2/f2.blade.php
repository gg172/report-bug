<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <script src="assets/js/bootstrap.js"></script>
    <title>f2</title>
    <style>
        select{
            margin-top: 30px;
            border: 2px solid black;
            width: 200px;
        }
        table, tr, th, td{
            border: 2px solid black;
        }
        a{
            text-decoration: none;
        }
        .modal-content{
            border-radius: 50px;
        }
        .modal-header{
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <select name="nama_tanaman" id="" class="bg-secondary">
            <option value="">SYSTEM</option>
        </select>
        <select name="nama_tanaman" id="" class="bg-secondary">
            <option value="">STATUS</option>
        </select>
        <table class="table text-center table-responsive mt-3">
            <thead class="bg-secondary">
              <tr>
                <th scope="col">NO</th>
                <th scope="col" class="col-2">CLINT</th>
                <th scope="col" class="col-2">SYSTEM</th>
                <th scope="col">PROBLEM</th>
                <th scope="col" class="col-2">OTHER</th>
                <th scope="col">ACTION</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>
                    <a href="" style="color: black;" data-bs-toggle="modal" data-bs-target="#identity">Alex</a>
                </td>
                <td>Otto</td>
                <td>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Asperiores commodi perspiciatis consequuntur <a href="" data-bs-toggle="modal" data-bs-target="#problem">readmore...</a></p>
                </td>
                <td class="text-start">
                    <p><b>DATE :</b></p>
                    <a>23/1/2023</a>
                    <p><b>STSTUS :</b></p>
                    <a>SUCCESS</a>
                </td>
                <td>
                    <a class="btn btn-secondary text-dark" href="{{ route('f3') }}">SOLVE THE PROBLEM</a>
                </td>
              </tr>
          </table>
          <!-- modal identity -->
            <div class="modal fade" id="identity" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content bg-dark-subtle">
                        <div class="">
                            <h1 class="modal-title fs-5 text-center pt-2" id="exampleModalLabel">IDENTITY</h1>
                        </div>
                        <div class="modal-body">
                            <p>Name : </p>
                            <p>Phone Number : </p>
                            <p>E-mail : </p>
                            <p>Egency : </p>
                        </div>
                        <div class="text-center pb-3">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
            </div>
          <!-- end identity -->
          <!-- modal readmore -->
          <div class="modal fade modal-xl" id="problem" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content bg-dark-subtle">
                    <div class="">
                        <h1 class="fs-5 text-center pt-2" id="exampleModalLabel">PROBLEM</h1>
                    </div>
                    <div class="modal-body p-5">
                        <h5>Description</h5>
                        <p>Lorem Ipsum hanyalah teks tiruan dari industri percetakan dan penyusunan huruf. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500-an, ketika seorang tukang cetak yang tidak dikenal mengambil kumpulan teks dan mengacaknya untuk membuat buku contoh huruf. Itu bertahan tidak hanya selama lima abad, tetapi juga lompatan ke pengaturan huruf elektronik, yang pada dasarnya tetap tidak berubah.                        </p>
                        <h5>Attachment</h5>
                        <div class="d-flex">
                            <div class="col-md-12 text-center">
                                <img class="img-fluid m-2"  src="https://i.pinimg.com/236x/7f/67/be/7f67be33232a4d32aaec5db95b2a2bf4.jpg" alt="">
                                <img class="img-fluid m-2"  src="https://i.pinimg.com/236x/7f/67/be/7f67be33232a4d32aaec5db95b2a2bf4.jpg" alt="">
                                <img class="img-fluid m-2"  src="https://i.pinimg.com/236x/7f/67/be/7f67be33232a4d32aaec5db95b2a2bf4.jpg" alt="">
                                <img class="img-fluid m-2"  src="https://i.pinimg.com/236x/7f/67/be/7f67be33232a4d32aaec5db95b2a2bf4.jpg" alt="">
                                <img class="img-fluid m-2"  src="https://i.pinimg.com/236x/7f/67/be/7f67be33232a4d32aaec5db95b2a2bf4.jpg" alt="">
                                <img class="img-fluid m-2"  src="https://i.pinimg.com/236x/7f/67/be/7f67be33232a4d32aaec5db95b2a2bf4.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end readmore -->
    </div>
</body>
</html>