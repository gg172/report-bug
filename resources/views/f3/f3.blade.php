<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- bootstrap css -->
    <link href="assets/css/bootstrap.css"  rel="stylesheet">
    <title>F3</title>
</head>
<body class="bg-body-secondary">
    <form class="bg-body-secondary">
        <div class="form-group row" style="margin-left: auto;">
          <label for="Username" class="col-sm-1 col-form-label">Username :</label>
          <div class="col-sm-1">
            <input type="text" readonly class="form-control-plaintext" id="Username" value="Alex">
          </div>
        </div>
        <div class="form-group row" style="margin-left: 1px;">
          <label for="System" class="col-sm-1 col-form-label">System :</label>
          <div class="col-sm-1">
            <input type="text" readonly class="form-control-plaintext" id="System" value="System">
          </div>
        </div>
      </form>


<!-- Button Trigger Model -->
      <div class="text-center">
        <h2 class="text-center">Check Problem</h2>
      <button type="button" class="btn btn-success btn-lg mb-5" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Problem
      </button>
    </div>
    
    


<!-- Modal -->
<div class="modal fade  modal-xl" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background-color: #c0c0c0;">
      <div class="modal-header" style="background-color: #808080 ; color:#fff;">
        <h1 class="modal-title" id="exampleModalLabel">
          Problem
        </h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body p-5">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi libero, earum repudiandae nulla nam deserunt ipsam eveniet, perspiciatis ab, voluptates voluptas quae obcaecati totam ducimus praesentium mollitia possimus accusantium saepe.</p>

        <h2 class="text-center">Attachement</h2>
        <div class="text-center d-flex">
            <div class="col-md-12">
        <img class="img-fluid mt-3"src="https://www.akseleran.co.id/blog/wp-content/uploads/2022/10/download-1.png" alt="">
        <img class="img-fluid mt-3"src="https://www.akseleran.co.id/blog/wp-content/uploads/2022/10/download-1.png" alt="">
        <img class="img-fluid mt-3"src="https://www.akseleran.co.id/blog/wp-content/uploads/2022/10/download-1.png" alt="">
        <img class="img-fluid mt-3"src="https://www.akseleran.co.id/blog/wp-content/uploads/2022/10/download-1.png" alt="">
        <img class="img-fluid mt-3"src="https://www.akseleran.co.id/blog/wp-content/uploads/2022/10/download-1.png" alt="">
    </div>
    </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

      
        <hr>
        <div class="form-group row" style="margin-left: 1px;">
          <label for="idticket" class="col-sm-1 col-form-label">ID Ticket :</label>
          <div class="col-sm-1">
            <input type="text" readonly class="form-control-plaintext" id="idticket" value="ID Ticket">
          </div>        
        </div>
      
 

<div class="body-timeline">
<div class="container py-2">

<!-- For demo purpose -->
<div class="row text-center text-black font-family-sans-serif">
    <div class="col-lg-0 mx-auto">
        <h1 class="display-0">Timeline</h1>
        </div>
    </div>
    <!-- End -->


    <div class="row">
        <div class="col-lg-7 mx-auto">
            
            <!-- Timeline -->
            <ul class="timeline">
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Client</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>12 Jan, 2023</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">TS</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>13 Jan, 2023</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper.</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Programmer (Process)</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>18 Jan, 2023</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Programmer (Finished)</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>19 Jan, 2023</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Ts(Check)</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>19 Jan, 2023</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Ts(Finished)</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>20 Jan, 2023</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
            </ul><!-- End -->

        </div>
    </div>
</div>
</div>
    <div class="form-group text-center bg-dark .bg-secondary.bg-gradient">
      <label for="exampleFormControlTextarea1" style="color: #fff;">Progress</label>
      <textarea class="form-control text-center" id="exampleFormControlTextarea1" rows="3"></textarea>
      <input class="btn btn-secondary" type="submit" value="Send">
    </div>
<script src="assets/js/bootstrap.js"></script>
     <style>
      .body-timeline{
      background-color:#8c8c8c;
}
    ul.timeline {
    list-style-type: none;
    position: relative;
    padding-left: 3rem;
}

 /* Timeline vertical line */
ul.timeline:before {
    content: ' ';
    background: #fff;
    display: inline-block;
    position: absolute;
    left: 16px;
    width: 4px;
    height: 100%;
    z-index: 400;
    border-radius: 1rem;
}

li.timeline-item {
    margin: 20px 0;
}

/* Timeline item arrow */
.timeline-arrow {
    border-top: 0.5rem solid transparent;
    border-right: 0.5rem solid #fff;
    border-bottom: 0.5rem solid transparent;
    display: block;
    position: absolute;
    left: 2rem;
}

/* Timeline item circle marker */
li.timeline-item::before {
    content: ' ';
    background: #ddd;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #fff;
    left: 11px;
    width: 14px;
    height: 14px;
    z-index: 400;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
}

.text-gray {
    color: #999;
}
     </style>
</body>
</html>






